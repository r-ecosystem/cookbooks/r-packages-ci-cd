# R Package Template fr CI/CD piepline.
# Copyright (C) 2016 Jean-François Rey <jean-françois.rey@inra.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#' @encoding UTF-8
#' @title R Package Template
#' @description A R package template to manage the package and the CI/CD piepline.
#' @aliases packagertemplate-package
#'
#' @author Jean-Francois Rey \email{jean-francois.rey@@inra.fr}
#'
#' Maintainer: Jean-Francois Rey \email{jean-francois.rey@@inra.fr}
#' @docType package
#' @name packagertemplate-package
#' @details \tabular{ll}{
#'          Package: \tab PackageRTemplate\cr
#'          Type: \tab Package\cr
#'          Version: \tab 0.1.0\cr
#'          Date: \tab 2019-11-05\cr
#'          License: \tab GPL (>=3)\cr
#'          }
#'
#' The PackageRTemplate package implements a R package template and how to manage CI/CD pipeline.
#' @keywords R package CI CD GitLab
#' @examples
#' \dontrun{
#' library("PackageRTemplate")
#' }
#' @import methods
#' @import graphics
#' @import stats
"_PACKAGE"

